package gripper.ftp;

import java.util.ArrayList;
import java.util.Objects;

public class PathConvert {
	public static String toServerPath(String root, String path) {
		String newpath = root + path;
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.toLowerCase().startsWith("win")) {
			newpath = newpath.replace("/", "\\");
			newpath = newpath.replace("\\\\", "\\");
		} else {
			newpath = newpath.replace("//", "/");
		}
		return newpath;
	}

	public static String toFtpPath(String root, String serverPath) {
		String newpath = serverPath.substring(root.length());
		newpath = "/" + newpath;
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.toLowerCase().startsWith("win")) {
			newpath = newpath.replace("\\", "/");
		}
		return newpath;
	}
	public static String shrink( String path){
		path=path.replace("//", "/");
		return path;
	}
	public static String serverPathFormat(String serverPath){
		String[] paths=serverPath.split("/");
		StringBuilder newPath=new StringBuilder();
		ArrayList<String> slist=new ArrayList<>();
		for(String s:paths){
			if(Objects.equals(".",s)){
				continue;
			}
			if(Objects.equals("..",s)){
				if(slist.size()>0){
					slist.remove(slist.size()-1);
				}
				continue;
			}
			slist.add(s);
		}
		for(String s:slist){
			newPath.append(s).append("/");
		}
		if(newPath.length()<=0){
			newPath.append("/");
		}
		slist.clear();
		return newPath.toString();
	}
	
	
}
