package gripper.ftp;

import java.io.IOException;
import java.io.Writer;

public class BasicLogger {
	static Writer writer;

	public static void setWriter(Writer writer) {
		BasicLogger.writer = writer;
	}

	public static void logInfo(String str) {
		if (writer == null) {
			System.out.println(str);
		} else {
			try {
				writer.write(str);
			} catch (IOException e) {
			}
		}
	}
}
