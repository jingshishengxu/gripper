package gripper.ftp.info;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FixSizeLinkedList<E> {
	private int capacity;
	private LinkedList<E> list;

	public FixSizeLinkedList(int capacity) {
		super();
		if (capacity < 0) {
			capacity = 0;
		}
		if (capacity > 65535) {
			capacity = 65535;
		}
		this.capacity = capacity;
		list = new LinkedList<>();
	}

	public boolean add(E t) {
		if (list.size() + 1 > capacity) {
			list.removeFirst();
		}
		return list.add(t);
	}

	public List<E> getAll() {
		List<E> result = new ArrayList<>(capacity);
		for (E e : list) {
			result.add(e);
		}
		return result;
	}

	public void clear() {
		list.clear();
	}
}
