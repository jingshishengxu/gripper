package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class OPTSHandle implements Handle {
	FtpSession ftpSession;
	public OPTSHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		if(line.contains("UTF8 ON")){
			os.write("200 OPTS UTF8 is set to ON.\r\n".getBytes());
		}else if(line.contains("MLST")){
			os.write("200 MLST OPTS Type;Size;Modify;Perm;\r\n".getBytes());
		}else{			
			os.write("200\r\n".getBytes());
		}
	}
}
