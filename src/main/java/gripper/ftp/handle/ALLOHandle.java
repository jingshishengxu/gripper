package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class ALLOHandle  implements Handle {
	FtpSession ftpSession;
	public ALLOHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("202 Command not implemented, superfluous at this site.\r\n".getBytes());
	}
}
