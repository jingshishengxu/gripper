package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;
import gripper.ftp.TransMode;

public class PASVHandle  implements Handle {
	FtpSession ftpSession;

	public PASVHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
    //227 Entering Passive Mode (192,168,0,222,4,0).
		String replayStr="227 Entering Passive Mode (#{ip},#{port})";
		replayStr=replayStr.replace("#{ip}", ftpSession.getIp());
		replayStr=replayStr.replace(".",",");
		ftpSession.setTransMode(TransMode.PASV);
		int port =ftpSession.createDataServer();
		ftpSession.ansyAccept();
		int a=port/256;
		int b=port%256;
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
		}
		replayStr=replayStr.replace("#{port}", a+","+b)+".\r\n";
		os.write(replayStr.getBytes());
		os.flush();
	}
	//a×256+y=new port
}
