package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class RESTHandle implements Handle {
	FtpSession ftpSession;

	public RESTHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("350 Requested file action pending further information.\r\n".getBytes());
		os.flush();
	}

}