package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class NOOPHandle implements Handle {
	FtpSession ftpSession;

	public NOOPHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write(("200 NOOP Command okay.").getBytes());
		os.flush();
	}
}