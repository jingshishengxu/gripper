package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class TYPEHandle  implements Handle {
	FtpSession ftpSession;
	public TYPEHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		if(line.contains("TYPE A")){
			os.write("200 Type set to A.\r\n".getBytes());
			os.flush();
		}else if(line.contains("TYPE I")){
			os.write("200 Type set to I.\r\n".getBytes());
			os.flush();
		}else{
			os.write("504 Command not implemented for that parameter.\r\n".getBytes());
			os.flush();
		}
	}
}
