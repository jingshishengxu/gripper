package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class PWDHandle implements Handle {
	FtpSession ftpSession;

	public PWDHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		//257 "/DRC" is current directory.
		os.write(("257 \""+ftpSession.currentDir+"\" is current directory.\r\n").getBytes());
		os.flush();
	}
}
