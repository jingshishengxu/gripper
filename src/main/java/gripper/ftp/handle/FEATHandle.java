package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class FEATHandle implements Handle {
	static final String featReplay="UTF8\r\n OPTS MODE;MLST;UTF8\r\n CLNT\r\n CSID Name; Version;\r\n HOST domain\r\n SITE PSWD;SET;ZONE;CHMOD;MSG;EXEC;HELP\r\n AUTH TLS;SSL;TLS-C;TLS-P;\r\n PBSZ\r\n PROT\r\n CCC\r\n SSCN\r\n RMDA directoryname\r\n DSIZ\r\n AVBL\r\n EPRT\r\n EPSV\r\n MODE Z\r\n THMB BMP|JPEG|GIF|TIFF|PNG max_width max_height pathname\r\n REST STREAM\r\n SIZE\r\n MDTM\r\n MDTM YYYYMMDDHHMMSS[+-TZ];filename\r\n MFMT\r\n MFCT\r\n MFF Create;Modify;\r\nMLST Type*;Size*;Create;Modify*;Perm;Win32.ea;Win32.dt;Win32.dl\r\n";
	FtpSession ftpSession;
	public FEATHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("211-Extensions supported\r\n".getBytes());
		os.write(featReplay.getBytes());
		os.write("211 End (for details use \"HELP commmand\" where command is the command of interest)\r\n".getBytes());
		os.flush();
	}

}
