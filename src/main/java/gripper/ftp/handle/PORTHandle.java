package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;
import gripper.ftp.TransMode;

public class PORTHandle implements Handle {
	FtpSession ftpSession;

	public PORTHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	//227 Entering Passive Mode (192,168,0,150,200,123)
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		String[] splited=line.split(" ");
		String[] ipports=splited[1].split(",");
		String ip=ipports[0]+"."+ipports[1]+"."+ipports[2]+"."+ipports[3];
		int port=Integer.parseInt(ipports[4])*256+Integer.parseInt(ipports[5]);
		os.write("200 PORT Command successful.\r\n".getBytes());
		os.flush();
		ftpSession.setTransMode(TransMode.PORT);
		ftpSession.ansyConnect(ip, port);
	}
}
