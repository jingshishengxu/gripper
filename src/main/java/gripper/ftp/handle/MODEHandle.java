package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class MODEHandle  implements Handle {
	FtpSession ftpSession;
	public MODEHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		if(line.toUpperCase().contains("MODE S")||line.toUpperCase().contains("MODE B")||line.toUpperCase().contains("MODE Z")){
			os.write("200 Noted.\r\n".getBytes());
			os.flush();
			return;
		}
		os.write("202 Command not implemented, superfluous at this site. \r\n".getBytes());
		os.flush();
	}
}
