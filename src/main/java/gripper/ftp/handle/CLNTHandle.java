package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class CLNTHandle  implements Handle {
	FtpSession ftpSession;
	public CLNTHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("200 Noted.\r\n".getBytes());
	}
}
