package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class SYSTHandle implements Handle {
	FtpSession ftpSession;

	public SYSTHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("215 UNIX Type: L8\r\n".getBytes());
		os.flush();
	}

}
