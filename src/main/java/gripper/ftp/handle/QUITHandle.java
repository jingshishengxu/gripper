package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class QUITHandle   implements Handle {
	FtpSession ftpSession;
	public QUITHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		os.write("221 Goodbye, closing session.\r\n".getBytes());
		os.flush();
		ftpSession.close();
	}
}
