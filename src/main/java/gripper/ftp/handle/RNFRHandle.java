package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.CmdType;
import gripper.ftp.FtpSession;
import gripper.ftp.PathConvert;

public class RNFRHandle  implements Handle {
	FtpSession ftpSession;
	public RNFRHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		final String[] splited = line.split(" ");
		String fileName = splited[1].trim();
		fileName = ftpSession.currentDir + "/" + fileName;
		fileName = PathConvert.toServerPath(ftpSession.getRootDir(), fileName);
		File file=new File(fileName);
		if(!file.exists()){
			os.write("550 Requested action not taken. File unavailable.\r\n".getBytes());
			os.flush();
			return;
		}
		ftpSession.fileNameRNFR=fileName;
		ftpSession.waitingCmd=CmdType.RNTO;
		os.write("350 Requested file action pending further information.\r\n".getBytes());
		os.flush();
	}

}
