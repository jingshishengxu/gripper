package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class CDUPHandle implements Handle {
	FtpSession ftpSession;

	public CDUPHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		String dir = ftpSession.currentDir;
		if ("/".equals(ftpSession.currentDir)) {
			os.write("200 Noted.\r\n".getBytes());
			os.flush();
			return;
		}
		if (dir.charAt(dir.length() - 1) == '/') {
			dir = dir.substring(0, dir.length() - 1);
		}
		dir = dir.substring(0, dir.lastIndexOf('/'));
		ftpSession.currentDir = dir;
		os.write("200 Noted.\r\n".getBytes());
		os.flush();
	}

}
