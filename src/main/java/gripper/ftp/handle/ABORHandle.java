package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class ABORHandle  implements Handle {
	FtpSession ftpSession;
	public ABORHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		///226 Closing data connection.
		ftpSession.abort.getAndSet(true);
		os.write("226 Closing data connection.\r\n".getBytes());
	}
}