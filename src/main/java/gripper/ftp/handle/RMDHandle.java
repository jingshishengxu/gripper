package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import gripper.ftp.FtpSession;
import gripper.ftp.PathConvert;

public class RMDHandle implements Handle {
	FtpSession ftpSession;

	public RMDHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		String dir=line.substring(line.indexOf(' '));
		dir=dir.trim();
		String path = PathConvert.toServerPath(ftpSession.getRootDir(), ftpSession.currentDir+"/"+dir);
		Path p=Paths.get(path);
		Files.deleteIfExists(p);
		os.write(("250 "+dir+" deleted.\r\n").getBytes());
		os.flush();
	}
}
