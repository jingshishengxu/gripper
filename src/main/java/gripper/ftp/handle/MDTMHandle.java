package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import gripper.ftp.FtpSession;
import gripper.ftp.PathConvert;

public class MDTMHandle implements Handle {
	FtpSession ftpSession;
	static SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	
	public MDTMHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}

	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		String dir=line.substring(line.indexOf(' '));
		dir=dir.trim();
		String path = PathConvert.toServerPath(ftpSession.getRootDir(), ftpSession.currentDir+"/"+dir);
		File file=new File(path);
		if(!file.exists()){
			os.write("550 file not exist.\r\n".getBytes());
			os.flush();
		}
		Date dt=new Date(file.lastModified());
		String dtStr=sdf.format(dt);
		os.write(("213 "+dtStr).getBytes());
		os.flush();
	}
}
