package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import gripper.ftp.FtpSession;

public class HELPHandle implements Handle {
	FtpSession ftpSession;
	public HELPHandle(FtpSession ftpSession) {
		this.ftpSession = ftpSession;
	}
	public void process(String line, BufferedReader br, OutputStream os) throws IOException {
		final String[] splited = line.split(" ");
		if (splited == null || splited.length < 2) {
			os.write("214 The following commands are recognized (* => unimplemented, + => extension).\r\n".getBytes());
			os.write("214     ABOR      EPSV      MKD       MSOM*     REST      STOR      XMKD.\r\n".getBytes());
			os.write("214     ACCT*     FEAT      MLFL*     NLST      RETR      STOU      XPWD.\r\n".getBytes());
			os.write("214     ALLO      HELP      MLSD      NOOP      RMD       STRU      XRMD.\r\n".getBytes());
			os.write("214     APPE      LIST      MLST      OPTS      RNFR      SYST.\r\n".getBytes());
			os.write("214     CLNT      MD5       MODE      PASV      SITE      USER.\r\n".getBytes());
			os.write("214     COMB      MDTM      MRCP*     PORT      SIZE      XCRC.\r\n".getBytes());
			os.write("214     CWD       MFCT      MRSQ*     PWD       SMNT      XCUP.\r\n".getBytes());
			os.write("214     DELE      MFF       MSAM*     QUIT      SPSV      XCWD.\r\n".getBytes());
			os.write("214     EPRT      MFMT      MSND*     REIN      STAT      XMD5.\r\n".getBytes());
			os.write("214\r\n".getBytes());
			os.flush();
			return;
		}
		String cmd=splited[1];
		if("HELP".equalsIgnoreCase(cmd)){
			os.write("214 Syntax: HELP [<sp><string>]\r\n".getBytes());
		}else{
			os.write("500 command not understand\r\n".getBytes());
		}
		os.flush();
	}
}
