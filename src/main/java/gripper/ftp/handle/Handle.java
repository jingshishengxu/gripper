package gripper.ftp.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

public interface Handle {
	void process(String line, BufferedReader br, OutputStream os) throws IOException;
}
