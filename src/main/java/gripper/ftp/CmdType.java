package gripper.ftp;

public enum CmdType {
	SYST, FEAT, CLNT, OPTS, PWD, PASV, MLSD, CWD, QUIT, TYPE, LIST, REST, RETR, STOR, MKD, MLST, MFMT, SIZE, DELE, CDUP, APPE, RNTO, RNFR, ABOR, ALLO, HELP, MODE, NLST, NOOP, RMD, SITE, STAT,MDTM,PORT
}
