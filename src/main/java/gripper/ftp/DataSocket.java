package gripper.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DataSocket {
	FtpSession ftpSession;
	Socket socket;

	public DataSocket(FtpSession ftpSession, Socket sock) {
		this.ftpSession = ftpSession;
		socket = sock;
	}

	public void close() {
		if (this.socket == null) {
			return;
		}
		try {
			this.socket.close();
		} catch (IOException e) {
		}
		this.socket = null;
	}
	public void flush(){
		try {
			socket.getOutputStream().flush();
		} catch (IOException e) {
		}
	}
	public OutputStream getOutputStream() throws IOException {
		return socket.getOutputStream();
	}

	public InputStream getInputStream() throws IOException {
		return socket.getInputStream();
	}
}
