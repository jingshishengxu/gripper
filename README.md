# gripper

#### 介绍
内嵌式ftp服务器。 在项目开发过程中需要搭建ftp服务器，外置部署独立的ftp服务器繁琐，功能复杂难用，将本工程嵌入java程序中，用线程启动，一个简易的ftp服务器即开始运行，并方便添加用户。
经FlashFxp 和WinSCP测试，主动模式，被动模式下均可以上传下载文件。
注意：只实现了明文传输，不支持加密。

#### 软件架构
纯java，可以集成到spring 等框架内，不依赖任何第三方库。


#### 安装教程

1.  用 maven 编译源码：mvn package install
2.  pom引入 包
    <dependency>
	    <groupId>gripper</groupId>
	    <artifactId>gripper-ftp</artifactId>
	    <version>1.0.0</version>
    </dependency>


#### 使用说明

    import gripper.ftp.FtpServer;

    FtpServer ftpServer=new FtpServer();
    ftpServer.start("username","password","D:\\ftpDocDir");

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
